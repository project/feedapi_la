<?php

/**
 * @file
 * FeedAPI Link Alter.
*/
function feedapi_la_admin_settings() {
  $form['feedapi_la_orig'] = array(
    '#type' => 'fieldset',
    '#title'  => 'Original Article Settings',
    '#collapsible'  => TRUE,
    '#collapsed'  => FALSE,
  );
  $form['feedapi_la_orig']['feedapi_la_orig_remove_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove link to original article'),
    '#default_value' => variable_get('feedapi_la_orig_remove_link', 0),
    '#description'  => t('Remove link to the original article. Note, you should only check this if the content of your node credits the original source.  If checked, 
will override any settings typed below.'),
  );    
  $form['feedapi_la_orig']['feedapi_la_orig_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Original article text'),
    '#default_value' => variable_get('feedapi_la_orig_text', 'Original article'),
    '#description'  => t('Enter text to replace the original article text'),
  );
  $form['feedapi_la_orig']['feedapi_la_orig_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use default text'),
    '#default_value' => variable_get('feedapi_la_orig_default', 0),
    '#description'  => t('Use the default original article text.  If checked, 
will override any replacement text typed in above. '),
  );
  $form['feedapi_la_feed'] = array(
    '#type' => 'fieldset',
    '#title'  => 'Feed Name Settings',
    '#collapsible'  => TRUE,
    '#collapsed'  => FALSE,
  ); 
  $form['feedapi_la_feed']['feedapi_la_feed_remove_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove "Feed:" link'),
    '#default_value' => variable_get('feedapi_la_feed_remove_link', 0),
    '#description'  => t('Remove the complete link to feed indexes.'),
  );   
  $form['feedapi_la_feed']['feedapi_la_feed_remove'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove "Feed:" text'),
    '#default_value' => variable_get('feedapi_la_feed_remove', 0),
    '#description'  => t('Remove the "Feed: " text that preceeds each link.'),
  );
  return system_settings_form($form);
}
