
FeedAPI Link Alter
=========================

FeedAPI Link Alter changes the text of the "Original article" and "Feed: [feed name]" 
links created by FeedAPI.

Requirements
=========================

FeedAPI and FeedAPI Node need to be installed and enabled

Installation
============

1.  Copy feedapi_la directory to your module directory and then enable on the admin
modules page.  
2.  Go to admin/build/modules and enable the module
3.  Settings to change can be found at admin/settings/feedapi_la

Contact
=========================

Current maintainer:
* Steve Dykman (quickcel) - http://drupal.org/user/30125